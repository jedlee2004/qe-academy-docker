#! /usr/bin/env bash

# Setting up theme
mkdir ~/.themes
cp usr/themesXFCE-Dark.tar.xz ~/.themes
cd .themes
tar -xf themesXFCE-Dark.tar.xz 
xfconf-query -c xsettings -p /Net/ThemeName -s XFCE-Dark
xfconf-query -c xfwm4 -p /general/theme -s XFCE-Dark

# Set Desktop Image
xfconf-query -c xfce4-desktop -l | grep last-image | while read path; do xfconf-query -c xfce4-desktop -p $path -s ${THEMES_DIR}/slalom.build.jpg; done