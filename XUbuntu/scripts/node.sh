# Ref: https://github.com/nodesource/distributions
NODE_VERSION=${NODE_VERSION:-15}
NODE_VERSION=${NODE_VERSION}
apt-get update -y && \
apt-get install -y sudo curl git xz-utils && \
curl -sL https://deb.nodesource.com/setup_${NODE_VERSION}.x | bash - && \
apt-get install -y nodejs
cd ${SCRIPT_DIR}; ${SCRIPT_DIR}/npm.proxy.setup.sh