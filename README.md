## Slalom QE Academy Pre-Configured Docker Environment
# Docker Images:
1. QE Academy:
    - Builds off of base image utilizing xubuntu
    - Adds the following to the image
        a. Chrome Browser
        b. Configures XFCE
        c. Python ( PIP )
        d. Java ( Maven / Gradle )
        e. Node ( NPM )
        f. Visual Studio Code
        g. Android Studio
    - sudo admin password: headless
    - vnc password qe_academy
2. Jenkins:
    - Builds off of base image jenkins/jenkins:lts
    - Copies over a plugin.txt to install pre-defined plugins
3. Postgres:
    - Runs a base image of postgres setting the DB name, user, and password for the instance
4. Git:
    - Runs a base image of gog/gog which is a self hosted git server
    - Requires setup with the postgres database

### Programming Languages
1. Java - OpenJDK 11: https://openjdk.java.net/projects/jdk/11/
2. Python 3 and Pip 3: https://www.python.org/download/releases/3.0/
3. NodeJs - LTS: https://nodejs.org/en/download/

### Code editor:
The container has Visual studio code installed on it but it is a better user experience to utilize the remoting functionality of Visual studio code from your machine and connect to the docker images.
- Overview and guides for remote development: https://code.visualstudio.com/docs/remote/remote-overview
- Install the Remote Development extension: https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.vscode-remote-extensionpack
- 
## GIT Source Code Management
Git source control server has been created to support the academy.
### Accessing the server
1. slalom.git:3000 => from within the docker container environment
2. localhost:3000 => from your local machine

# Jenkins Server in Docker Image:
Jenkins server has been created to support the academy.
### Accessing the server
1. slalom.jenkins:8080 => from within the docker container environment
2. localhost:8080 => from your local machine
3. Retrieve default password: ```docker exec jenkins-local cat /var/jenkins_home/secrets/initialAdminPassword```

## Starting up the docker containers for the QE Academy Environment
```docker compose up -d --build```
## Stopping the docker containers
```docker compose down```

## VNC Access:
To load up the Linux virtual environment:
1. Open desired browser
2. Navigate to localhost:6901
3. Click noVNC Full Client
4. Click Connect
5. Input password: qe_academy

# Uploading / Pulling image to / from ECR:
1. Login via azure aws login: ```aws-azure-login --profile qe_academy``` assuming setup beforehand
2. Authenticate with AWS ECR Registry: https://docs.aws.amazon.com/AmazonECR/latest/userguide/registry_auth.html
    ```aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin 537129989794.dkr.ecr.us-east-1.amazonaws.com```
3. Tag docker image: use the ecr repository URI as tag name
    ```docker tag qe-academy-jenkins:latest 537129989794.dkr.ecr.us-east-1.amazonaws.com/slalom-qe-academy```
4. Use docker cli to push to ecr: get url from ECr Repository
    ```docker push 537129989794.dkr.ecr.us-east-1.amazonaws.com/slalom-qe-academy```
5. Pulling the image from AWS ECR:
    ex: ```docker pull aws_account_id.dkr.ecr.us-west-2.amazonaws.com/amazonlinux:latest```
    actual: ```docker pull 537129989794.dkr.ecr.us-east-1.amazonaws.com/slalom-qe-academy:latest```

UBUNTU JENKINS IMAGE:
PULL IMAGE: ```docker pull 537129989794.dkr.ecr.us-east-1.amazonaws.com/jenkins-ubuntu:latest```
RUN IMAGE: ```docker run -dit -p 5901:5901 -p 6901:6901 --memory=6g  --name aws-ubuntu-jenkins  537129989794.dkr.ecr.us-east-1.amazonaws.com/jenkins-ubuntu```

## Deploy Environment via AWS ECS:
1. Create an ECS context: ```docker context create ecs ${ecsContextName}```
2. View context: ```docker context ls```
3. Set Context so all subsequent commands target Amazon ECS:
    - ```docker context use ${ecsContextName}```